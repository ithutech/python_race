GUIDE:

```
git clone https://gitlab.com/ithutech/python_race.git
cd python_race
tar xf libs2.tgz 
cd libs2
make
cp ./libdarknet.so ../src/ithutech/model/darknet.so
cd ..
catkin_make

source devel/setup.bash
roslaunch ithutech ithutech.launch

Open simulator with team: ithutech - ws://127.0.0.1:9005
```
